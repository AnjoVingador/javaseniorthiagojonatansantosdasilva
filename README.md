# JavaSeniorThiagoJonatanSantosDaSilva

# Banco
Foi usado o MONGODB na versão 3.6.2.

# Framework Persistência
Foi usado o Hibernate OGM para auxiliar a persistência e busca de dados no MONGODB.

# Criação
Foi usado o "Thorntail" para auxiliar na criação do sistema. https://thorntail.io/

# Executar
Para rodar basta executar o comando do maven "mvn package", logo após executar "java -jar restful-endpoint-thorntail.jar" na pasta do .jar gerado. Se faz necessário o mongodb em execução na porta padrão. A API pode ser acessada pela URL http://localhost:8080/api/colaboradores 

