package br.mobicare.avaliacao.bo.impl;

import java.util.List;

import br.mobicare.avaliacao.bo.ConsultaBo;
import br.mobicare.avaliacao.dao.ConsultaDAO;
import br.mobicare.avaliacao.exception.BasicException;
import br.mobicare.avaliacao.exception.ObjetoNaoEncontradoException;

public abstract class TesteMobicareConsultaBo<T> implements ConsultaBo<T> {

	protected abstract ConsultaDAO<T> getDao();

	public List<T> listar() {
		return getDao().listar();
	}


	public T buscar(Object id) throws BasicException {
		T result = getDao().buscar(id);
		if(result == null)
			throw new ObjetoNaoEncontradoException();
		
		return result;
	}
	
}
