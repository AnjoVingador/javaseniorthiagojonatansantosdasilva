package br.mobicare.avaliacao.bo;

import java.util.List;

import br.mobicare.avaliacao.exception.BasicException;

public interface ConsultaBo<T> {
    
   public List<T> listar();

   public T buscar(Object id) throws BasicException;
   
}
