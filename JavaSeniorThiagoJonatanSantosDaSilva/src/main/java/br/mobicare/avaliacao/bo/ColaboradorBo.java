package br.mobicare.avaliacao.bo;

import br.mobicare.avaliacao.model.Colaborador;

public interface ColaboradorBo extends GenericBo<Colaborador> {

}
