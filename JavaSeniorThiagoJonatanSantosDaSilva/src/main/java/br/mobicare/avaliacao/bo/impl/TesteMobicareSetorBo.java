package br.mobicare.avaliacao.bo.impl;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.mobicare.avaliacao.bo.SetorBo;
import br.mobicare.avaliacao.dao.ColaboradorDAO;
import br.mobicare.avaliacao.dao.ConsultaDAO;
import br.mobicare.avaliacao.dao.SetorDAO;
import br.mobicare.avaliacao.model.Setor;

@RequestScoped
public class TesteMobicareSetorBo extends TesteMobicareConsultaBo<Setor> implements SetorBo {

	@Inject
	private SetorDAO dao;
	
	@Inject
	private ColaboradorDAO colaboradorDao;
	

	@Override
	protected ConsultaDAO<Setor> getDao() {
		return dao;
	}


	@Override
	public List<Setor> listar() {
		List<Setor> setores = super.listar();
		setores.forEach(setor -> {
			
			setor.setColaboradores(colaboradorDao.listarPorSetor(setor.getSequencial()));
		});
		return setores;
	}


}
