package br.mobicare.avaliacao.bo.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.mobicare.avaliacao.bo.ColaboradorBo;
import br.mobicare.avaliacao.dao.ColaboradorDAO;
import br.mobicare.avaliacao.dao.GenericDAO;
import br.mobicare.avaliacao.exception.BasicException;
import br.mobicare.avaliacao.exception.ObjetoEncontradoException;
import br.mobicare.avaliacao.model.Colaborador;
import br.mobicare.avaliacao.util.ConstantsMessage;
import br.mobicare.avaliacao.util.MessageUtil;

@RequestScoped
public class TesteMobicareColaboradorBo extends TesteMobicareGenericBo<Colaborador> implements ColaboradorBo {

	@Inject
	private ColaboradorDAO dao;
	
	
	public TesteMobicareColaboradorBo() {
	}

	@Override
	public void incluir(Colaborador colaborador) throws BasicException {
		if(dao.existePorCpf(colaborador.getCpf())) {
			ObjetoEncontradoException exception = new ObjetoEncontradoException();
			exception.getDetalhe().addErro("cpf", MessageUtil.getMessage(ConstantsMessage.ERROR_00003.name()));
			throw exception;
		}
		
		super.incluir(colaborador);
	}


	@Override
	protected GenericDAO<Colaborador> getDao() {
		return dao;
	}


}
