package br.mobicare.avaliacao.bo;

import br.mobicare.avaliacao.exception.BasicException;

public interface GenericBo<T> extends ConsultaBo<T> {
    
   public void incluir(T obj) throws BasicException;
   
   public void excluir(T obj);
   
}
