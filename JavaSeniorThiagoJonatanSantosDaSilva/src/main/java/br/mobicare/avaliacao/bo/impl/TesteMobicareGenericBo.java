package br.mobicare.avaliacao.bo.impl;

import br.mobicare.avaliacao.bo.GenericBo;
import br.mobicare.avaliacao.dao.GenericDAO;
import br.mobicare.avaliacao.exception.BasicException;

public abstract class TesteMobicareGenericBo<T> extends TesteMobicareConsultaBo<T> implements GenericBo<T> {

    protected abstract GenericDAO<T> getDao();

    public void incluir(T obj) throws BasicException {
        getDao().incluir(obj);
    }

    public void alterar(T obj) {
        getDao().alterar(obj);
    }

    public void excluir(T obj) {
		getDao().excluir(obj);
    }

}
