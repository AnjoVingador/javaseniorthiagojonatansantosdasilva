package br.mobicare.avaliacao.dao;

import java.util.List;

public interface ConsultaDAO<T> {

	T buscar(Object id);

	List<T> listar();

}
