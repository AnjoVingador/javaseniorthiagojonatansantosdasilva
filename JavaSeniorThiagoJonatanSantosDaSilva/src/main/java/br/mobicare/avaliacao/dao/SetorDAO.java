package br.mobicare.avaliacao.dao;

import br.mobicare.avaliacao.model.Setor;

public interface SetorDAO extends GenericDAO<Setor> {

}
