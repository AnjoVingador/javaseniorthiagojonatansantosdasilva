package br.mobicare.avaliacao.dao.jpa;

import javax.enterprise.context.RequestScoped;

import br.mobicare.avaliacao.dao.SetorDAO;
import br.mobicare.avaliacao.model.Setor;

@RequestScoped
public class JpaSetorDAO extends JpaGenericDao<Setor> implements SetorDAO {

	public JpaSetorDAO() {
		super(Setor.class);
	}

}
