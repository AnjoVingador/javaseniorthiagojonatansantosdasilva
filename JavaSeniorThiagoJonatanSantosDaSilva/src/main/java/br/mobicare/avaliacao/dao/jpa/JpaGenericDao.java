package br.mobicare.avaliacao.dao.jpa;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.TypedQuery;

import br.mobicare.avaliacao.dao.GenericDAO;

public abstract class JpaGenericDao<T> implements GenericDAO<T> {

	@Inject
	protected EntityManager em;

	protected Class<T> entityClass;

	public JpaGenericDao(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	protected EntityManager getEntityManager() {
		return em;
	}

	public void incluir(T entity) {
		em.getTransaction().begin();
		em.persist(entity);
		em.getTransaction().commit();

	}

	public void alterar(T entity) {
		em.getTransaction().begin();
		em.merge(entity);
		em.getTransaction().commit();
	}

	public void excluir(T entity) {
		em.getTransaction().begin();
		em.remove(entity);
		em.getTransaction().commit();
	}

	public T buscar(Object id) {

		String nameField = findIdField(entityClass);

		try {
			TypedQuery<T> query = em.createQuery("from " + entityClass.getSimpleName() + " where " + nameField + "=:id",
					entityClass);
			query.setParameter("id", id);
			return query.getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			return null;
		}
	}

	public List<T> listar() {
		TypedQuery<T> query = em.createQuery("from " + entityClass.getSimpleName(), entityClass);
		return query.getResultList();
	}

	private String findIdField(Class<T> cls) {
		for (Field field : cls.getDeclaredFields()) {
			String name = field.getName();
			Annotation[] annotations = field.getDeclaredAnnotations();
			for (int i = 0; i < annotations.length; i++) {
				if (annotations[i].annotationType().equals(Id.class)) {
					return name;
				}
			}
		}
		return null;
	}

}
