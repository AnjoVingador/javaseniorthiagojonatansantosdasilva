package br.mobicare.avaliacao.dao;

public interface GenericDAO<T> extends ConsultaDAO<T> {
	
	void incluir(T entity);

	void alterar(T entity);

	void excluir(T entity);
    
}
