package br.mobicare.avaliacao.dao;

import java.util.List;

import br.mobicare.avaliacao.model.Colaborador;

public interface ColaboradorDAO extends GenericDAO<Colaborador> {

	boolean existePorCpf(String cpf);
	
	List<Colaborador> listarPorSetor(Integer setor);
	

}
