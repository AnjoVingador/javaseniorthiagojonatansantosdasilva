package br.mobicare.avaliacao.dao.jpa;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.mobicare.avaliacao.dao.ColaboradorDAO;
import br.mobicare.avaliacao.model.Colaborador;

@RequestScoped
public class JpaColaboradorDAO extends JpaGenericDao<Colaborador> implements ColaboradorDAO {

	public JpaColaboradorDAO() {
		super(Colaborador.class);
	}

	@Override
	public boolean existePorCpf(String cpf) {
		try {
			Query query = em.createNativeQuery("db." + entityClass.getSimpleName() + ".count({_id: '"+cpf+"'})");
			return (Long)query.getSingleResult() > 0;
		} catch (javax.persistence.NoResultException e) {
			return false;
		}
	}

	@Override
	public List<Colaborador> listarPorSetor(Integer setor) {
		TypedQuery<Colaborador> query = em.createQuery("from Colaborador where idSetor = :setor ", entityClass);
		query.setParameter("setor", setor);
		
		return query.getResultList();
	}

}
