package br.mobicare.avaliacao.util;



import java.util.ResourceBundle;
import java.util.StringTokenizer;

public class MessageUtil {

	
	private static ResourceBundle properties = null;
	
	static{
		
		properties = ResourceBundle.getBundle("messages");
	}

	
	public static String getMessage(String key) {
		try {
			return properties.getString(key);
		} catch (Exception e) {
			System.err.print(e.getMessage());
			// SegurancaLog.registrarError(new PropriedadesMd()
			// ,e.getMessage());
		}
		return null;
	}

	public static String[] getStringArray(String key) {
		String[] retorno = null;
		try {
			String array = properties.getString(key);
			StringTokenizer st = new StringTokenizer(array, ",");
			int tokens = st.countTokens();
			retorno = new String[tokens];

			for (int i = 0; i < tokens; i++) {
				retorno[i] = st.nextToken();
			}

		} catch (Exception e) {
			System.err.print(e.getMessage());
			// SegurancaLog.registrarError(new PropriedadesMd()
			// ,e.getMessage());
			retorno = null;
		}
		return retorno;
	}

	public static int getInt(String key) {
		try {
			return new Integer(properties.getString(key)).intValue();
		} catch (Exception e) {
			System.err.print(e.getMessage());
			// SegurancaLog.registrarError(new PropriedadesMd()
			// ,e.getMessage());
		}
		return 0;
	}

	public static boolean getBoolean(String key) {
		try {
			return new Boolean(properties.getString(key)/*getProperties().getString(key)*/).booleanValue();
		} catch (Exception e) {
			System.err.print(e.getMessage());
			// SegurancaLog.registrarError(new PropriedadesMd()
			// ,e.getMessage());
		}
		return false;
	}

}
