package br.mobicare.avaliacao.util;

public enum ConstantsMessage {
	ERROR_00000(400), ERROR_00001(404), ERROR_00002(500), ERROR_00003(400);
	
	private int codigoHtml;
	
	private ConstantsMessage(int codigoHtml) {
		this.codigoHtml = codigoHtml;
	}
	
	public int getCodigo() {
		return codigoHtml;
	}
}
