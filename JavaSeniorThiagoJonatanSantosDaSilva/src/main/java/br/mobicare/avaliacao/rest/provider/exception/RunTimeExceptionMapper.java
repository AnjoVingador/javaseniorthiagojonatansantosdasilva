package br.mobicare.avaliacao.rest.provider.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import br.mobicare.avaliacao.util.ConstantsMessage;
import br.mobicare.avaliacao.util.MessageUtil;

@Provider
public class RunTimeExceptionMapper implements ExceptionMapper<RuntimeException> {
	
private static final Logger LOGGER = Logger.getLogger(RunTimeExceptionMapper.class);
	
	@Override
	public Response toResponse(RuntimeException exception) {
		Aviso erroDTO = new Aviso(ConstantsMessage.ERROR_00002.getCodigo(), MessageUtil.getMessage(ConstantsMessage.ERROR_00002.name()), exception.getMessage());
		
		LOGGER.info(exception.getMessage(), exception);
		
		return Response.status(erroDTO.getCode()).entity(erroDTO).type(MediaType.APPLICATION_JSON).build();
	}
	

}
