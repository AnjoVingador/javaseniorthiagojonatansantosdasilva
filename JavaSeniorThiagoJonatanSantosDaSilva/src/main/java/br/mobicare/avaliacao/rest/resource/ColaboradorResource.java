package br.mobicare.avaliacao.rest.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import br.mobicare.avaliacao.bo.ColaboradorBo;
import br.mobicare.avaliacao.bo.SetorBo;
import br.mobicare.avaliacao.exception.BasicException;
import br.mobicare.avaliacao.model.Colaborador;
import br.mobicare.avaliacao.model.Setor;
import br.mobicare.avaliacao.rest.dto.ColaboradorDTO;
import br.mobicare.avaliacao.rest.dto.SetorDTO;

@Path("/colaboradores")
@Produces({ "application/json; charset=UTF-8", MediaType.APPLICATION_XML })
@Consumes({ "application/json; charset=UTF-8", MediaType.APPLICATION_XML })
public class ColaboradorResource {
	
	@Inject
	private ColaboradorBo bo;
	
	@Inject
	private SetorBo setorBo;
	
	@POST
	public Response inserir(@Valid Colaborador colaborador) throws BasicException {

		bo.incluir(colaborador);
		
		URI uri = UriBuilder
				.fromPath(
						"colaboradores/{cpf}")
				.build(colaborador.getCpf());

		return Response.created(uri).entity(colaborador).build();
	}
	
	@GET
	@Path("{cpf}")
	public Response buscar(@PathParam("cpf") String cpf) throws BasicException {
		Colaborador colaborador = bo.buscar(cpf);
		
		return Response.ok(colaborador).build();
	}
	
	@DELETE
	@Path("{cpf}")
	public Response remover(@PathParam("cpf") String cpf) throws BasicException {
		Colaborador colaborador = bo.buscar(cpf);
		
		bo.excluir(colaborador);
		
		return Response.ok().build();
	}
	
	
	@GET
	public Response listarPorSetor() {
		List<Setor> setores = setorBo.listar();
		
		GenericEntity<List<SetorDTO>> entity = new GenericEntity<List<SetorDTO>>(converter(setores)) {
		};

		return Response.ok(entity).build();
	}

	
	private List<SetorDTO> converter(List<Setor> setores) {
		List<SetorDTO> setoresDTO = new ArrayList<>();
		
		setores
		.forEach(setor -> {
			
			if(!setor.getColaboradores().isEmpty()) {
				SetorDTO setorDTO = new SetorDTO(setor.getSequencial(), setor.getDescricao());
				setoresDTO.add(setorDTO);
				
				setor.getColaboradores()
				.forEach(colaborador -> setorDTO.getColaboradores().add(new ColaboradorDTO(colaborador.getNome(), colaborador.getEmail())));
			}
		});
		return setoresDTO;
	}

}
