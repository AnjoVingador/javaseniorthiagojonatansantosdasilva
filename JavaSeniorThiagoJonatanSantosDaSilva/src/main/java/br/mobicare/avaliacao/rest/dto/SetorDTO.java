package br.mobicare.avaliacao.rest.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SetorDTO implements Serializable {
	
	private static final long serialVersionUID = 5571072824544433930L;

	private Integer id;
	
	private String descricao;
	
	private List<ColaboradorDTO> colaboradores;

	public SetorDTO() {
		this.colaboradores = new ArrayList<>();
	}

	public SetorDTO(Integer id, String descricao) {
		this();
		this.id = id;
		this.descricao = descricao;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer sequencial) {
		this.id = sequencial;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<ColaboradorDTO> getColaboradores() {
		return colaboradores;
	}

	public void setColaboradores(List<ColaboradorDTO> colaboradores) {
		this.colaboradores = colaboradores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetorDTO other = (SetorDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
