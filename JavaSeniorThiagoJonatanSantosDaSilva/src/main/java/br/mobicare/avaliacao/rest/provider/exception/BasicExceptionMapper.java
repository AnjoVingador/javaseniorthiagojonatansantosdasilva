package br.mobicare.avaliacao.rest.provider.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import br.mobicare.avaliacao.exception.BasicException;

@Provider
public class BasicExceptionMapper implements ExceptionMapper<BasicException> {

	private static final Logger LOGGER = Logger.getLogger(BasicExceptionMapper.class);

	@Override
	public Response toResponse(BasicException exception) {
		LOGGER.info(exception.getMessage(), exception);

		return Response.status(exception.getDetalhe().getCode()).entity(exception.getDetalhe()).type(MediaType.APPLICATION_JSON).build();
	}
}
