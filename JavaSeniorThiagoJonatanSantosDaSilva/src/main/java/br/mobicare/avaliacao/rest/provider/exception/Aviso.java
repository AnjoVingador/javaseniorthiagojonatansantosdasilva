package br.mobicare.avaliacao.rest.provider.exception;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(value=Include.NON_NULL)
public class Aviso implements Serializable {
	
	private static final long serialVersionUID = -175992891002084437L;
	
	private int code;
	private String mensagem;
	private String detalhe;
	private Map<String, String> cadastroErro;

	public Aviso() {
		this.cadastroErro = new HashMap<>();
	}

	public Aviso(String mensagem) {
		this();
		this.mensagem = mensagem;
	}
	
	public Aviso(int code, String mensagem) {
		this();
		this.code = code;
		this.mensagem = mensagem;
	}

	public Aviso(int code, String mensagem, String detalhe) {
		this();
		this.code = code;
		this.mensagem = mensagem;
		this.detalhe = detalhe;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public String addErro(String campo, String mensagem) {
		return cadastroErro.put(campo, mensagem);
	}
}
