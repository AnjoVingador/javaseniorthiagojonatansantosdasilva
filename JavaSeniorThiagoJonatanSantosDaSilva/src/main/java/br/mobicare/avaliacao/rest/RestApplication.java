package br.mobicare.avaliacao.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import br.mobicare.avaliacao.rest.provider.exception.BasicExceptionMapper;
import br.mobicare.avaliacao.rest.provider.exception.RunTimeExceptionMapper;
import br.mobicare.avaliacao.rest.provider.exception.ValidationExceptionMapper;
import br.mobicare.avaliacao.rest.resource.ColaboradorResource;

@ApplicationPath("/api")
public class RestApplication extends Application {
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		
		classes.add(ValidationExceptionMapper.class);	
		classes.add(RunTimeExceptionMapper.class);
		classes.add(BasicExceptionMapper.class);
		classes.add(ColaboradorResource.class);	

		return classes;
	}

}
