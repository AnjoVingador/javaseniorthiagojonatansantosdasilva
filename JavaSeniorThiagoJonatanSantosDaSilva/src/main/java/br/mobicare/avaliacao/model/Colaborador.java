package br.mobicare.avaliacao.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Colaborador implements Serializable {

	private static final long serialVersionUID = 5867877194919548434L;
	
	@Id
	@NotNull(message = "{cadastro.campo.notnull}")
	@Size(min = 11, max = 11, message = "{cadastro.campo.size}")
	private String cpf;
	
	@NotNull(message = "{cadastro.campo.notnull}")
	private String nome;
	
	@NotNull(message = "{cadastro.campo.notnull}")
	@Size(min = 10, max = 11, message = "{cadastro.campo.size}")
	private String telefone;
	
	@NotNull(message = "{cadastro.campo.notnull}")
	@Pattern(regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "${cadastro.campo.email}")
	private String email;
	
	@NotNull(message = "{cadastro.campo.notnull}")
	@ManyToOne
	@JoinColumn(name = "setor", referencedColumnName = "sequencial")
	private Setor setor;
	
	@XmlTransient
	@Column(name = "setor", insertable = false, updatable = false)
	private Integer idSetor;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String eEmail) {
		this.email = eEmail;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Integer getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(Integer idSetor) {
		this.idSetor = idSetor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colaborador other = (Colaborador) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		return true;
	}

}
