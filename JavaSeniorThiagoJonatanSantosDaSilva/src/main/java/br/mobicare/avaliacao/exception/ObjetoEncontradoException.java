package br.mobicare.avaliacao.exception;

import java.io.Serializable;

import br.mobicare.avaliacao.util.ConstantsMessage;

public class ObjetoEncontradoException extends BasicException implements Serializable {

	private static final long serialVersionUID = 934886159117435161L;
	
	public ObjetoEncontradoException() {
		super(ConstantsMessage.ERROR_00001);
	}
}
