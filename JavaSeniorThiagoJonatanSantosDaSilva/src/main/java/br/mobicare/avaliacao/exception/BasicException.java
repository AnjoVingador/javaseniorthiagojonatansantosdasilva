package br.mobicare.avaliacao.exception;

import br.mobicare.avaliacao.rest.provider.exception.Aviso;
import br.mobicare.avaliacao.util.ConstantsMessage;
import br.mobicare.avaliacao.util.MessageUtil;

public class BasicException extends Exception {

	private static final long serialVersionUID = -26924287209119579L;

	private Aviso detalhe;
	
	protected BasicException(ConstantsMessage codigoErro) {
		this(MessageUtil.getMessage(codigoErro.name()), codigoErro.getCodigo());
	}
	
	public BasicException(final String message) {
		super(message);
	}
	
	public BasicException(final Throwable cause) {
		super(cause);
	}

	public BasicException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public BasicException(final Exception e) {
		super(e);
	}

	public BasicException(final String message, int code) {
		super(message);
		this.detalhe = new Aviso(code, message);
	}
	
	public BasicException() {
		super();
	}

	public BasicException(final String msg, final Exception e) {
		super(msg, e);
	}

	public Aviso getDetalhe() {
		return detalhe;
	}

}